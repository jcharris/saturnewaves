xmin = -200;
xmax = +200;
ymin = -200;
ymax = +200;

lc = 2;
//lf = 10;

r0 = 5;
r1 = 6.25;
l1 = 51.75;

x0 = 0; y0 = 0;
x01 = x0-r0;   y01 = y0;
x02 = x0+r0/2; y02 = y0-Sqrt(3)/2*r0;
x03 = x0+r0/2; y03 = y0+Sqrt(3)/2*r0;

x1 =  -l1; y1 = 0;
x2 = l1/2; y2 = l1*Sqrt(3)/2;
x3 = l1/2; y3 =-l1*Sqrt(3)/2;

x11 = x1-r1; y11 = y1;
x12 = x1;    y12 = y1-r1;
x13 = x1+r1; y13 = y1;
x14 = x1;    y14 = y1+r1;

x21 = x2+r1/2;         y21 = y2+Sqrt(3)/2*r1;
x22 = x2-Sqrt(3)/2*r1; y22 = y2+r1/2;
x23 = x2-r1/2;         y23 = y2-Sqrt(3)/2*r1;
x24 = x2+Sqrt(3)/2*r1; y24 = y2-r1/2;

x31 = x3+r1/2;         y31 = y3-Sqrt(3)/2*r1;
x32 = x3+Sqrt(3)/2*r1; y32 = y3+r1/2;
x33 = x3-r1/2;         y33 = y3+Sqrt(3)/2*r1;
x34 = x3-Sqrt(3)/2*r1; y34 = y3-r1/2;

Point( 1) = {x0, y0,  0,lc};
Point( 2) = {x01,y01, 0,lc};
Point( 3) = {x02,y02, 0,lc};
Point( 4) = {x03,y03, 0,lc};

Point( 5) = {x1, y1, 0,lc};
Point( 6) = {x11,y11,0,lc};
Point( 7) = {x12,y12,0,lc};
Point( 8) = {x13,y13,0,lc};
Point( 9) = {x14,y14,0,lc};

Point(10) = {x2, y2, 0,lc};
Point(11) = {x21,y21,0,lc};
Point(12) = {x22,y22,0,lc};
Point(13) = {x23,y23,0,lc};
Point(14) = {x24,y24,0,lc};

Point(15) = {x3, y3, 0,lc};
Point(16) = {x31,y31,0,lc};
Point(17) = {x32,y32,0,lc};
Point(18) = {x33,y33,0,lc};
Point(19) = {x34,y34,0,lc};

Point(20) = {xmax,ymax,0};
Point(21) = {xmin,ymax,0};
Point(22) = {xmin,ymin,0};
Point(23) = {xmax,ymin,0};

Point(24) = {-r1/Sqrt(3),+r1,0,lc};
Point(25) = {-r1/Sqrt(3),-r1,0,lc};
Point(26) = {r1*2/Sqrt(3),0,0,lc};

Circle( 1) = { 2, 1, 3};
Circle( 2) = { 3, 1, 4};
Circle( 3) = { 4, 1, 2};

Circle( 4) = { 6, 5, 7};
Circle( 5) = { 7, 5, 8};
Circle( 6) = { 8, 5, 9};
Circle( 7) = { 9, 5, 6};

Circle( 8) = {11,10,12};
Circle( 9) = {12,10,13};
Circle(10) = {13,10,14};
Circle(11) = {14,10,11};

Circle(12) = {16,15,17};
Circle(13) = {17,15,18};
Circle(14) = {18,15,19};
Circle(15) = {19,15,16};

Line(16) = {20,21}; Transfinite Line(16) = 40;
Line(17) = {21,22}; Transfinite Line(17) = 40;
Line(18) = {22,23}; Transfinite Line(18) = 40;
Line(19) = {23,20}; Transfinite Line(19) = 40;

Line(20) = {24,9};
Line(21) = {7,25};
Line(22) = {25,19};
Line(23) = {17,26};
Line(24) = {26,14};
Line(25) = {12,24};

Translate {0,0,-15} { Line{1:25};}

Curve Loop(1) = {1:3};
Curve Loop(2) = {4:7};
Curve Loop(3) = {8:11};
Curve Loop(4) = {12:15};
Curve Loop(5) = {16:19};
Curve Loop(6) = {20,-6,-5,21,22,-14,-13,23,24,-10,-9,25};
Curve Loop(7) = {20,7,4,21,22,15,12,23,24,11,8,25};

Surface(1) = {1};
Surface(2) = {2};
Surface(3) = {3};
Surface(4) = {4};
Plane Surface(5) = {6,1};
Plane Surface(6) = {5,7};

Translate{0,0,+7} {Duplicata{Surface{5};}}
Extrude{0,0,+7} {Surface{6}; Layers{4}; Recombine;}
Extrude{0,0,+8} {Surface{26,123}; Layers{4}; Recombine;}
Extrude{0,0,-185} {Surface{1:6}; Layers{4}; Recombine;}

Physical Volume("internal") = {1:9};
Physical Surface("Outer") = {62,66,70,74,463,467,471,475,221,225,229,233};
Physical Surface("FS") = {282,200};
Physical Surface("Bottom") = {524,442,365,343,321,299};
Physical Surface("BodyBottom") = {1,2,3,4,5,26,78,82,86,90,94,98,102,106,110,114,118,122};
Physical Surface("BodySides") = {147,151,163,167,179,183,191,195,199,241,245,257,261,273,277};

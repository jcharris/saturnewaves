// Written by Jeffrey C. Harris, ENPC (jeffrey.harris@enpc.fr)
//
// Modified from code: https://github.com/dutykh/BabenkoSolitaryWave
// Original version by Denys Dutykh and Didier Clamond, 2014, MIT license
//
// The MIT License (MIT)
// 
// Copyright (c) 2014 Denys Dutykh
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef __SOLITARY_H__
#define __SOLITARY_H__

#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <stdlib.h>

// FFTs
void _fft(complex buf[], complex out[], int n, int step);
void fft(complex buf[], complex buf2[], int n);
void ifft(complex buf[], complex buf2[], int n);

// Special sinc function
double sinc(double x);

// cubic Hermite interpolation
double interp1(double *xvals, double *yvals, double x, int N);

// Data
typedef struct {
  double Ampl;
  double Fr;
  int N;
  double dxi;
  double *Ceta, *dexi, *xs;
  double *etas, *phis, *us, *vs;
} Solitary;

// Global info
extern Solitary s;

// Functions
void solitary_init(double ampl);
double solitary_froude();
double solitary_eta(double x);
double solitary_u(double x, double y);
double solitary_v(double x, double y);
double solitary_p(double x, double y);
void solitary_free();

#endif


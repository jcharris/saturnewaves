!-------------------------------------------------------------------------------

!                      Code_Saturne version 6.0-alpha
!                      --------------------------
! This file is part of Code_Saturne, a general-purpose CFD tool.
!
! Copyright (C) 1998-2019 EDF S.A.
!
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
! details.
!
! You should have received a copy of the GNU General Public License along with
! this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
! Street, Fifth Floor, Boston, MA 02110-1301, USA.

!-------------------------------------------------------------------------------

!===============================================================================
! Purpose:
! -------

!> \file cs_user_modules.f90
!>
!> \brief User-defined module: it allows to create any user array.
!>
!> See \subpage cs_user_modules for examples.
!>
!> This file is compiled before all other user Fortran files.
!> To ensure this, it must not be renamed.
!>
!> The user may define an arbitrary number of modules here, even though
!> only one is defined in the example.
!
!> \cond DOXYGEN_SHOULD_SKIP_THIS

!-------------------------------------------------------------------------------

module user_module

  !=============================================================================


use cs_c_bindings
  implicit none

  !=============================================================================

  double precision, save :: d, waveht, gravity, twave, c, ddx

  ! Example: allocatable user arrays

  double precision, dimension(:,:), allocatable :: velf
  double precision, dimension(:,:), allocatable :: meshwf
  double precision, dimension(:), allocatable :: presf

contains

  !=============================================================================

  ! Allocate arrays

  subroutine init_user_module(nfabor, gx, gy, gz)

    use cstphy, only: xyzp0, ro0, p0
    use mesh, only: cdgfbo
    ! Arguments

    integer, intent(in) :: nfabor
    double precision, intent(in) :: gx, gy, gz

    ! Local variables

    integer :: err = 0

    integer ifac

    ! outlet velocity (m/s)
    !d      = 1.d0                  ! still water depth
    !gravity= sqrt(gx**2.d0 + gy**2.d0 + gz**2.d0)
    !c = 1.24999114837592
    
    c = notebook_parameter_value_by_name('Cout')

    if (.not.allocated(velf)) then
         allocate(velf(3,nfabor), stat=err)
    endif
    if(err /= 0) then
      write(*,*) "error allocating local memory velf"
    endif

    if (.not.allocated(meshwf)) then
         allocate(meshwf(3,nfabor), stat=err)
    endif
    if(err /= 0) then
      write(*,*) "error allocating local memory velf"
    endif


    if (err .eq. 0 .and. .not.allocated(presf)) then
        allocate(presf(nfabor), stat=err)
    endif
    if(err /= 0) then
      write(*,*) "error allocating local memory presf"
    endif

    if (err /= 0) then
      write (*, *) "Error allocating array."
      call csexit(err)
    endif

    do ifac = 1, nfabor
      velf(1, ifac) = 0.d0
      velf(2, ifac) = 0.d0
      velf(3, ifac) = 0.d0
      meshwf(1, ifac) = 0.d0
      meshwf(2, ifac) = 0.d0
      meshwf(3, ifac) = 0.d0
      presf(ifac) = p0 + ro0*( gx*(cdgfbo(1,ifac) - xyzp0(1))  &
                             + gy*(cdgfbo(2,ifac) - xyzp0(2))  &
                             + gz*(cdgfbo(3,ifac) - xyzp0(3)))
    enddo

    return

  end subroutine init_user_module

  !=============================================================================

  ! Free related arrays

  subroutine finalize_user_module

    if (allocated(velf)) then
      deallocate(velf)
    endif

    if (allocated(meshwf)) then
      deallocate(meshwf)
    endif

    if (allocated(presf)) then
      deallocate(presf)
    endif

  end subroutine finalize_user_module

  !=============================================================================

end module user_module

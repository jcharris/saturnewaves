// Written by Jeffrey C. Harris, ENPC (jeffrey.harris@enpc.fr)
//
// Modified from code: https://github.com/dutykh/BabenkoSolitaryWave
// Original version by Denys Dutykh and Didier Clamond, 2014, MIT license
//
// The MIT License (MIT)
// 
// Copyright (c) 2014 Denys Dutykh
// 
// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice shall be included in all
// copies or substantial portions of the Software.
//
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include <math.h>
#include <complex.h>
#include <stdio.h>
#include <stdlib.h>

#include "solitary.h"

// Pi for C99
#define M_PI 3.14159265358979323846

// Global info
Solitary s;

// FFTs
void _fft(complex buf[], complex out[], int n, int step) {
  if (step < n) {
    _fft(out, buf, n, 2*step);
    _fft(out+step, buf+step, n, 2*step);
    for (int i=0; i<n; i += 2*step) {
      complex t = cexp(-I*M_PI*i/n)*out[i+step];
      buf[i/2]     = out[i]+t;
      buf[(i+n)/2] = out[i]-t;
    }
  }
}

void fft(complex buf[], complex buf2[], int n) {
  complex out[n];
  for (int i=0; i<n; i++)
    out[i] = buf[i];
  _fft(buf, out, n, 1);
  for (int i=0; i<n; i++)
    buf2[i] = buf[i];
}

void ifft(complex buf[], complex buf2[], int n) {
  complex out[n];
  for (int i=0; i<n; i++) {
    buf[i] = conj(buf[i]);
    out[i] = buf[i];
  }
  _fft(buf, out, n, 1);
  for (int i=0; i<n; i++)
    buf2[i] = conj(buf[i]) / ((double) n);
}

// Special sinc function
double sinc(double x) {
  if (x==0)
    return 1.0;
  else
    return sin(x)/x;
}

// cubic Hermite interpolation
double interp1(double *xvals, double *yvals, double x, int N) {
  int i = 2;
  int j = N-3;
  if (x < *xvals+i) {
    return *(yvals+i);
  } else if (x > *(xvals+j)) {
    return *(yvals+j);
  } else {
    while (j-i>1) {
      int k = (int) (i+j)/2;
      if (x > *(xvals+k)) {
        i = k;
      } else {
        j = k;
      }
    }
  }
  double eps = 1.0e-6;
  double f = 1.0e+6;
  double s = 0.5;
  while (f>eps) {
    f = -x
      +*(xvals+i-1)*(-0.5*s*(1.0-s)*(1.0-s))
      +*(xvals+i)*((1.0-s)*(1.0-s)*(2.0*s+1.0) - 0.5*s*s*(s-1.0))
      +*(xvals+i+1)*(s*s*(3.0-2.0*s)+0.5*s*(1.0-s)*(1.0-s))
      +*(xvals+i+2)*(0.5*s*s*(s-1.0));
    double df =
      +*(xvals+i-1)*(-1.5*s*s+2.0*s-0.5)
      +*(xvals+i)*(4.5*s*s-5*s)
      +*(xvals+i+1)*(-4.5*s*s+4*s+0.5)
      +*(xvals+i+2)*(s*(1.5*s-1.0));
    s = s - f/df;
  }
  return *(yvals+i+1)*s + *(yvals+i)*(1.0-s);
}

// Functions
void solitary_init(double ampl) {
  s.Ampl = ampl;
  s.N = 16384; //
  s.Ceta = (double*) malloc(sizeof(double)*s.N);
  s.dexi = (double*) malloc(sizeof(double)*s.N);
  s.etas = (double*) malloc(sizeof(double)*s.N);
  s.xs   = (double*) malloc(sizeof(double)*s.N);
  double tol = 1.0e-15;
  double F2 = 1.0 + s.Ampl - 0.05*s.Ampl*s.Ampl
    - 3.0/70.0*s.Ampl*s.Ampl*s.Ampl;
  double kd;
  double kd_low  = 0.0;
  double kd_high = 1.5;
  while (kd_high-kd_low>=tol) {
    kd = (kd_high+kd_low)/2.0;
    if (sinc(kd)-F2*cos(kd)==0.0)
      break;
    else if ((sinc(kd)-F2*cos(kd))
	     *(sinc(kd_low)-F2*cos(kd_low))<0.0)
      kd_high = kd;
    else
      kd_low = kd;
  }
  double L = 17.0*log(10.0)/kd;
  s.dxi = 2.0*L/((double) s.N);
  double xi[s.N], k[s.N];
  complex COP[s.N], IOP[s.N];
  for (int i=0; i<s.N; i++) {
    xi[i] = ((double) (1-s.N/2+i))*s.dxi;
    if (i<s.N/2)
      k[i] = ((double) i)*M_PI/L;
    else
      k[i] = ((double) (i-s.N))*M_PI/L;
    COP[i] = (complex) (k[i]/tanh(k[i]));
    IOP[i] = (complex) (-I/tanh(k[i]));
  }
  COP[0] = 1.0;
  IOP[0] = 0.0;
  for (int i=0; i<s.N; i++) {
    s.etas[i] = s.Ampl/cosh(0.5*kd*xi[i])/cosh(0.5*kd*xi[i]);
  }
  double err = 1.0e9;
  complex tmp[s.N],tmp2[s.N];
  int iter=0;
  while (err>tol) {
    iter++;
    //printf("iter: %d\n",iter);
    complex eta_hat[s.N],eta2_hat[s.N];
    complex eta2[s.N];
    for (int i=0; i<s.N; i++) {
      tmp[i] = (complex) s.etas[i];
    }
    fft(tmp,eta_hat,s.N);
    for (int i=0; i<s.N; i++) {
      eta2[i] = (complex) s.etas[i]*s.etas[i];
    }
    fft(eta2,eta2_hat,s.N);
    for (int i=0; i<s.N; i++) {
      tmp[i] = (complex) COP[i] * eta_hat[i];
    }
    ifft(tmp,tmp2,s.N);
    for (int i=0; i<s.N; i++) {
      s.Ceta[i] = creal(tmp2[i]);
    }
    double M0 = 0.0;
    double P0 = 0.0;
    for (int i=0; i<s.N; i++) {
      M0 += s.dxi*s.etas[i]*(1.0+s.Ceta[i]);
      P0 += 0.5*s.dxi*(1.0+s.Ceta[i])*s.etas[i]*s.etas[i];
    }
    F2 = 1.0 + 3.0*P0/M0;
    complex LOP[s.N], LOI[s.N];
    for (int i=0; i<s.N; i++) {
      LOP[i] = COP[i]*F2-1.0;
      LOI[i] = 1.0/LOP[i];
    }
    complex Leta_hat[s.N], Nl_hat[s.N];
    for (int i=0; i<s.N; i++) {
      Leta_hat[i] = LOP[i]*eta_hat[i];
    }
    for (int i=0; i<s.N; i++) {
      tmp[i] = (complex) s.etas[i]*s.Ceta[i];
    }
    fft(tmp,tmp2,s.N);
    for (int i=0; i<s.N; i++) {
      Nl_hat[i] = 0.5*COP[i]*eta2_hat[i] + tmp2[i];
    }
    complex S1 = 0.0;
    complex S2 = 0.0;
    for (int i=0; i<s.N; i++) {
      S1 += eta_hat[i]*Leta_hat[i];
      S2 += eta_hat[i]*Nl_hat[i];
    }
    complex S = (S1/S2)*(S1/S2); // pow(...)
    double eta1[s.N];
    for (int i=0; i<s.N; i++) {
      tmp[i] = S*LOI[i]*Nl_hat[i];
    }
    ifft(tmp,tmp2,s.N);
    for (int i=0; i<s.N; i++) {
      eta1[i] = creal(tmp2[i]);
    }
    double a = 0.0; // or negative?
    for (int i=0; i<s.N; i++) {
      if (eta1[i]>a)
	a = eta1[i];
    }
    for (int i=0; i<s.N; i++) {
      eta1[i] = s.Ampl*eta1[i]/a;
    }
    err = 0.0;
    for (int i=0; i<s.N; i++) {
      if (fabs(eta1[i]-s.etas[i])>err)
	err = fabs(eta1[i]-s.etas[i]);
    }
    for (int i=0; i<s.N; i++) {
      s.etas[i] = eta1[i];
    }
    //    printf("err: %f\n", err);
  }
  s.Fr = sqrt(F2);
  //  printf("Fr: %f\n",s.Fr);
  for (int i=0; i<s.N; i++) {
    tmp[i] = (complex) s.etas[i];
  }
  fft(tmp,tmp2,s.N);
  for (int i=0; i<s.N; i++) {
    tmp[i] = COP[i]*tmp2[i];
  }
  ifft(tmp,tmp2,s.N);
  for (int i=0; i<s.N; i++) {
    s.Ceta[i] = creal(tmp2[i]);
  }
  for (int i=0; i<s.N; i++) {
    tmp[i] = (complex) s.etas[i];
  }
  fft(tmp,tmp2,s.N);
  for (int i=0; i<s.N; i++) {
    tmp[i] = I*k[i]*tmp2[i];
  }
  ifft(tmp,tmp2,s.N);
  for (int i=0; i<s.N; i++) {
    s.dexi[i] = creal(tmp2[i]);
  }
  double etaMean = 0.0;
  for (int i=0; i<s.N; i++) {
    etaMean += s.etas[i]/((double) s.N);
  }
  double etazero[s.N];
  for (int i=0; i<s.N; i++) {
    etazero[i] = s.etas[i]-etaMean;
  }
  for (int i=0; i<s.N; i++) {
    tmp[i] = (complex) etazero[i];
  }
  fft(tmp,tmp2,s.N);
  for (int i=0; i<s.N; i++) {
    tmp[i] = IOP[i]*tmp2[i];
  }
  ifft(tmp,tmp2,s.N);
  for (int i=0; i<s.N; i++) {
    s.xs[i] = (1.0+etaMean)*xi[i] + creal(tmp2[i]);
  }
  s.us   = (double*) malloc(sizeof(double)*s.N);
  s.vs   = (double*) malloc(sizeof(double)*s.N);
  s.phis = (double*) malloc(sizeof(double)*s.N);
  for (int i=0; i<s.N; i++) {
    double qs = (1.0+s.Ceta[i])*(1.0+s.Ceta[i]) + s.dexi[i]*s.dexi[i];
    s.us[i]   = s.Fr - s.Fr*(1.0+s.Ceta[i])/qs;
    s.vs[i]   =      - s.Fr*(    s.dexi[i])/qs;
    s.phis[i] = s.Fr*(s.xs[i]-xi[i]);
  }
}

double solitary_froude() {
  return s.Fr;
}

double solitary_eta(double x) {
  return interp1(s.xs,s.etas,x,s.N);
}

double solitary_u(double x, double y) {
  complex zj = x + I*y;
  complex W = 0.0;
  for (int i=0; i<s.N; i++) {
    complex zs = s.xs[i] + I*s.etas[i];
    complex dzsm1 = s.Ceta[i] + I*s.dexi[i];
    W += I*0.5*s.Fr*s.dxi/M_PI
      *(dzsm1/(zs-zj)-conj(dzsm1)/(conj(zs)-2*I-zj));
  }
  if (fabs(y-interp1(s.xs,s.etas,x,s.N))<0.001) {
    return interp1(s.xs,s.us,x,s.N);
  } else {
    return creal(W);
  }
}

double solitary_v(double x, double y) {
  complex zj = x + I*y;
  complex W = 0.0;
  for (int i=0; i<s.N; i++) {
    complex zs = s.xs[i] + I*s.etas[i];
    complex dzsm1 = s.Ceta[i] + I*s.dexi[i];
    W += I*0.5*s.Fr*s.dxi/M_PI
      *(dzsm1/(zs-zj)-conj(dzsm1)/(conj(zs)-2*I-zj));
  }
  if (fabs(y-interp1(s.xs,s.etas,x,s.N))<0.001) {
    return interp1(s.xs,s.vs,x,s.N);
  } else {
    return -cimag(W);
  }
}

double solitary_p(double x, double y) {
  complex zj = x + I*y;
  complex W = 0.0;
  for (int i=0; i<s.N; i++) {
    complex zs = s.xs[i] + I*s.etas[i];
    complex dzsm1 = s.Ceta[i] + I*s.dexi[i];
    W += I*0.5*s.Fr*s.dxi/M_PI
      *(dzsm1/(zs-zj)-conj(dzsm1)/(conj(zs)-2*I-zj));
  }
  if (fabs(y-interp1(s.xs,s.etas,x,s.N))<0.001) {
    return s.Fr*interp1(s.xs,s.us,x,s.N)
      - 0.5*(interp1(s.xs,s.us,x,s.N)*interp1(s.xs,s.us,x,s.N)
	     +interp1(s.xs,s.vs,x,s.N)*interp1(s.xs,s.vs,x,s.N));
  } else {
    // remove hydrostatic part...
    return s.Fr*creal(W) - 0.5*cabs(W)*cabs(W);// - cimag(zj);
  }
}

void solitary_free() {
  free(s.Ceta);
  free(s.dexi);
  free(s.xs);
  free(s.etas);
  free(s.phis);
  free(s.us);
  free(s.vs);
}


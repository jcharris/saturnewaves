!-------------------------------------------------------------------------------

!                      Code_Saturne version 6.0-alpha
!                      --------------------------
! This file is part of Code_Saturne, a general-purpose CFD tool.
!
! Copyright (C) 1998-2019 EDF S.A.
!
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
! details.
!
! You should have received a copy of the GNU General Public License along with
! this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
! Street, Fifth Floor, Boston, MA 02110-1301, USA.

!-------------------------------------------------------------------------------

!===============================================================================
! Purpose:
! -------

!> \file cs_user_extra_operations.f90
!>
!> \brief This function is called at the end of each time step, and has a very
!>  general purpose
!>  (i.e. anything that does not have another dedicated user subroutine)
!>
!> See \subpage cs_user_extra_operations_examples and
!> \subpage cs_user_extra_operations-nusselt_calculation for examples.
!>
!-------------------------------------------------------------------------------

!-------------------------------------------------------------------------------
! Arguments
!______________________________________________________________________________.
!  mode           name          role                                           !
!______________________________________________________________________________!
!> \param[in]     nvar          total number of variables
!> \param[in]     nscal         total number of scalars
!> \param[in]     dt            time step (per cell)
!_______________________________________________________________________________

subroutine cs_f_user_extra_operations &
 ( nvar   , nscal  ,                                              &
   dt     )

!===============================================================================

!===============================================================================
! Module files
!===============================================================================

use paramx
use dimens, only: ndimfb
use pointe
use numvar
use optcal
use cstphy
use cstnum
use entsor
use lagran
use parall
use period
use ppppar
use ppthch
use ppincl
use mesh
use field
use field_operator
use turbomachinery
use cs_c_bindings
use user_module

!===============================================================================

implicit none

! Arguments

integer          nvar   , nscal

double precision dt(ncelet)

! Local variables

integer          iel    , ielg   , ifac   , ifacg  , ivar
integer          iel1   , iel2   , ieltsm
integer          iortho , impout
integer          inc    , iccocg
integer          nswrgp , imligp , iwarnp
integer          iutile , iclvar , iii
integer          ipcrom , ipcvst , iflmas , iflmab , ipccp, ipcvsl
integer          iscal
integer          ii     , nbr    , irangv , irang1 , npoint
integer          imom   , ipcmom , idtcm
integer          itab(3), iun
integer          ncesmp
integer          ilelt  , nlelt

double precision xfor(3), xyz(3), xabs, xu, xv, xw, xk, xeps

double precision eta, uin,win,arg,dwdz
double precision courant
double precision visclc,aa,ab,rome
double precision zmax, clocal,distx


integer, allocatable, dimension(:) :: lstelt

double precision, dimension(:,:), pointer :: cvar_vel
double precision, dimension(:,:), pointer :: cvar_meshw
double precision, dimension(:), pointer :: cpro_pres_tot
double precision, dimension(:), pointer :: cpro_viscl
double precision, dimension(:), pointer :: bpro_rho

!===============================================================================


!===============================================================================
! 1.  Initialization
!===============================================================================

call field_get_val_v(ivarfl(iu), cvar_vel)
!call field_get_val_v(ivarfl(iuma), cvar_meshw)
call field_get_val_s(iprtot, cpro_pres_tot)

call field_get_val_s(iprpfl(iviscl), cpro_viscl)
call field_get_val_s(ibrom, bpro_rho)

! Allocate a temporary array for cells or interior/boundary faces selection
allocate(lstelt(max(ncel,nfac,nfabor)))
!===============================================================================
! Save in user arrays of the boundary faces for pressure and velocity
!===============================================================================

!convective outlet
call getfbr('6',nlelt,lstelt)
do ilelt = 1, nlelt
  ifac = lstelt(ilelt)
  iel = ifabor(ifac)
  visclc  = cpro_viscl(iel)
  rome    = bpro_rho(ifac)
  courant = c*dt(iel)/abs(cdgfbo(1,ifac)-xyzcen(1,iel))
  ! evaluation of the coeff
  aa=1.d0/(1.d0+courant)
  ab=courant/(1.d0+courant)
  ! update of the values
  ! NB velf and pres are init to 0 in user_init
  velf(1,ifac) = aa*velf(1,ifac) + ab* cvar_vel(1, iel)
  velf(2,ifac) = aa*velf(2,ifac) + ab* cvar_vel(2, iel)
  velf(3,ifac) = aa*velf(3,ifac) + ab* cvar_vel(3, iel)
  ! store the dynamic pressure
  presf(ifac)  = aa*presf(ifac)  + ab* cpro_pres_tot(iel)
enddo
!===============================================================================
! Deallocation of the user array at the last interation
if (ntcabs.ge.ntmabs) call finalize_user_module
! Deallocate the temporary array
deallocate(lstelt)


return
end subroutine cs_f_user_extra_operations

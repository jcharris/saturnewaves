!-------------------------------------------------------------------------------

!                      Code_Saturne version 6.0-alpha
!                      --------------------------
! This file is part of Code_Saturne, a general-purpose CFD tool.
!
! Copyright (C) 1998-2018 EDF S.A.
!
! This program is free software; you can redistribute it and/or modify it under
! the terms of the GNU General Public License as published by the Free Software
! Foundation; either version 2 of the License, or (at your option) any later
! version.
!
! This program is distributed in the hope that it will be useful, but WITHOUT
! ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
! FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
! details.
!
! You should have received a copy of the GNU General Public License along with
! this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
! Street, Fifth Floor, Boston, MA 02110-1301, USA.

!-------------------------------------------------------------------------------

!===============================================================================
! Arguments
!______________________________________________________________________________.
!  mode           name          role                                           !
!______________________________________________________________________________!
!> \param[in]     nvar          total number of variables
!> \param[in]     nscal         total number of scalars
!> \param[out]    icodcl        boundary condition code:
!>                               - 1 Dirichlet
!>                               - 2 Radiative outlet
!>                               - 3 Neumann
!>                               - 4 sliding and
!>                                 \f$ \vect{u} \cdot \vect{n} = 0 \f$
!>                               - 5 smooth wall and
!>                                 \f$ \vect{u} \cdot \vect{n} = 0 \f$
!>                               - 6 rough wall and
!>                                 \f$ \vect{u} \cdot \vect{n} = 0 \f$
!>                               - 9 free inlet/outlet
!>                                 (input mass flux blocked to 0)
!>                               - 13 Dirichlet for the advection operator and
!>                                    Neumann for the diffusion operator
!> \param[in]     itrifb        indirection for boundary faces ordering
!> \param[in,out] itypfb        boundary face types
!> \param[out]    izfppp        boundary face zone number
!> \param[in]     dt            time step (per cell)
!> \param[in,out] rcodcl        boundary condition values:
!>                               - rcodcl(1) value of the dirichlet
!>                               - rcodcl(2) value of the exterior exchange
!>                                 coefficient (infinite if no exchange)
!>                               - rcodcl(3) value flux density
!>                                 (negative if gain) in w/m2 or roughness
!>                                 in m if icodcl=6
!>                                 -# for the velocity \f$ (\mu+\mu_T)
!>                                    \gradt \, \vect{u} \cdot \vect{n}  \f$
!>                                 -# for the pressure \f$ \Delta t
!>                                    \grad P \cdot \vect{n}  \f$
!>                                 -# for a scalar \f$ cp \left( K +
!>                                     \dfrac{K_T}{\sigma_T} \right)
!>                                     \grad T \cdot \vect{n} \f$
!_______________________________________________________________________________

subroutine cs_f_user_boundary_conditions &
 ( nvar   , nscal  ,                                              &
   icodcl , itrifb , itypfb , izfppp ,                            &
   dt     ,                                                       &
   rcodcl )

!===============================================================================

!===============================================================================
! Module files
!===============================================================================

use paramx
use numvar
use optcal
use cstphy
use cstnum
use entsor
use parall
use period
use ihmpre
use ppppar
use ppthch
use coincl
use cpincl
use ppincl
use ppcpfu
use atincl
use atsoil
use ctincl
use cs_fuel_incl
use mesh
use field
use turbomachinery
use iso_c_binding
use cs_c_bindings
use user_module

!===============================================================================

implicit none

! Arguments

integer          nvar   , nscal

integer          icodcl(nfabor,nvar)
integer          itrifb(nfabor), itypfb(nfabor)
integer          izfppp(nfabor)

double precision dt(ncelet)
double precision rcodcl(nfabor,nvar,3)

! Local variables

integer          ifac, iel, ii, ivar
integer          izone
integer          ilelt, nlelt
integer          icon
integer          inod
double precision uref2
double precision rhomoy, xdh, xustar2
double precision xitur
double precision xkent, xeent

double precision eta, uin,win,arg,dwdz
double precision courant
double precision visclc,aa,ab,rome
double precision zmax, clocal,distx

integer, allocatable, dimension(:) :: lstelt
double precision, dimension(:), pointer :: cpro_viscl
double precision, dimension(:), pointer :: bpro_rho


!===============================================================================


!===============================================================================
! 1.  Initialization
!===============================================================================

! Allocate a temporary array for boundary faces selection
allocate(lstelt(nfabor))

call field_get_val_s(iprpfl(iviscl), cpro_viscl)
call field_get_val_s(ibrom, bpro_rho)


! indicate if the convective boundary is active or not
! =1 or =0
icon=1

zmax = 0.d0

!===============================================================================
! 2.  Assign boundary conditions to boundary faces here

!     One may use selection criteria to filter boundary case subsets
!       Loop on faces from a subset
!         Set the boundary condition for each face
!===============================================================================


! Radiative outlet
call getfbr('6',nlelt,lstelt)

do ilelt = 1, nlelt

  ifac = lstelt(ilelt)
  iel = ifabor(ifac)
  itypfb(ifac) =iparoi !isolib
  visclc  = cpro_viscl(iel)
  rome    = bpro_rho(ifac)
  courant = c*dt(iel)/abs(cdgfbo(1,ifac)-xyzcen(1,iel))

  flush(nfecra)

  ! --- New code for convective outlet

  ! For the pressure
  icodcl(ifac,ipr) = 2

  ! The CFL number and the value of the DYNAMIC pressure of time step n
  ! are required in rcodcl(.,.,2) and rcodcl(.,.,1)
  rcodcl(ifac,ipr,2)= courant
  rcodcl(ifac,ipr,1)= presf(ifac)

  flush(nfecra)

  ! For velocities
  icodcl(ifac,iu)  = 2
  rcodcl(ifac,iu,2)= courant
  rcodcl(ifac,iu,1) = velf(1, ifac)

  icodcl(ifac,iv)  = 2
  rcodcl(ifac,iv,2)= courant
  rcodcl(ifac,iv,1) = velf(2, ifac)

  icodcl(ifac,iw)  = 2
  rcodcl(ifac,iw,2)= courant
  rcodcl(ifac,iw,1) = velf(3, ifac)

enddo

!----
! Formats
!----

!----
! End
!----

! Deallocate the temporary array
deallocate(lstelt)

return
end subroutine cs_f_user_boundary_conditions

!> \section examples Examples
!>   Several examples are provided
!>   \ref cs_user_boundary_conditions_examples "here".

Tdelay=8;
height=0.6;
[zs,ws,fs,SWP] = SolitaryGravityAmplitude(height);
Fr = SWP(1);
t0 = Tdelay + 20/Fr;
tfwd = Tdelay + 40/Fr;

THEORY = [real(zs)/Fr + t0, imag(zs)];
THEORY = THEORY(THEORY(:,1)>15 & THEORY(:,1)<45,:);
save('theory.txt','THEORY','-ascii');

printf('LEGACY:\n\n');

printf('5 VERTICAL LEVELS\n');
tmp=load('probes_LEGACY_5.dat');
time = tmp(:,1);
elev = tmp(:,2);
harr = max(elev(time<tfwd));
tarr = time(find(elev==harr));
hbwd = max(abs(elev(time>=tfwd)));
printf('   Arrival height: %12f ; %% error: %6f\n',harr,100*(harr-height)/height);
printf('     Arrival time: %12f ; %% error: %6f\n',tarr-Tdelay,100*(tarr-t0)/(20/Fr));
printf('Reflection height: %12f ; %% error: %6f\n\n',hbwd,100*(hbwd)/height);

printf('10 VERTICAL LEVELS\n');
tmp=load('probes_LEGACY_10.dat');
time = tmp(:,1);
elev = tmp(:,2);
harr = max(elev(time<tfwd));
tarr = time(find(elev==harr));
hbwd = max(abs(elev(time>=tfwd)));
printf('   Arrival height: %12f ; %% error: %6f\n',harr,100*(harr-height)/height);
printf('     Arrival time: %12f ; %% error: %6f\n',tarr-Tdelay,100*(tarr-t0)/(20/Fr));
printf('Reflection height: %12f ; %% error: %6f\n\n',hbwd,100*(hbwd)/height);

printf('20 VERTICAL LEVELS\n');
tmp=load('probes_LEGACY_20.dat');
time = tmp(:,1);
elev = tmp(:,2);
harr = max(elev(time<tfwd));
tarr = time(find(elev==harr));
hbwd = max(abs(elev(time>=tfwd)));
printf('   Arrival height: %12f ; %% error: %6f\n',harr,100*(harr-height)/height);
printf('     Arrival time: %12f ; %% error: %6f\n',tarr-Tdelay,100*(tarr-t0)/(20/Fr));
printf('Reflection height: %12f ; %% error: %6f\n\n',hbwd,100*(hbwd)/height);

printf('CDO:\n\n');

printf('5 VERTICAL LEVELS\n');
tmp=load('probes_CDO_5.dat');
time = tmp(:,1);
elev = tmp(:,2);
harr = max(elev(time<tfwd));
tarr = time(find(elev==harr));
hbwd = max(abs(elev(time>=tfwd)));
printf('   Arrival height: %12f ; %% error: %6f\n',harr,100*(harr-height)/height);
printf('     Arrival time: %12f ; %% error: %6f\n',tarr-Tdelay,100*(tarr-t0)/(20/Fr));
printf('Reflection height: %12f ; %% error: %6f\n\n',hbwd,100*(hbwd)/height);

printf('10 VERTICAL LEVELS\n');
tmp=load('probes_CDO_10.dat');
time = tmp(:,1);
elev = tmp(:,2);
harr = max(elev(time<tfwd));
tarr = time(find(elev==harr));
hbwd = max(abs(elev(time>=tfwd)));
printf('   Arrival height: %12f ; %% error: %6f\n',harr,100*(harr-height)/height);
printf('     Arrival time: %12f ; %% error: %6f\n',tarr-Tdelay,100*(tarr-t0)/(20/Fr));
printf('Reflection height: %12f ; %% error: %6f\n\n',hbwd,100*(hbwd)/height);

printf('20 VERTICAL LEVELS\n');
tmp=load('probes_CDO_20.dat');
time = tmp(:,1);
elev = tmp(:,2);
harr = max(elev(time<tfwd));
tarr = time(find(elev==harr));
hbwd = max(abs(elev(time>=tfwd)));
printf('   Arrival height: %12f ; %% error: %6f\n',harr,100*(harr-height)/height);
printf('     Arrival time: %12f ; %% error: %6f\n',tarr-Tdelay,100*(tarr-t0)/(20/Fr));
printf('Reflection height: %12f ; %% error: %6f\n\n',hbwd,100*(hbwd)/height);

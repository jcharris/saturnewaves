#!/bin/sh
(
# DOWNLOAD SOURCE
cd ~
rm code_saturne-master.zip
wget https://github.com/code-saturne/code_saturne/archive/refs/heads/master.zip -O code_saturne-master.zip
rm -r code_saturne-master/
unzip code_saturne-master.zip
cd code_saturne-master
./sbin/bootstrap
cd ..
# COMPILE SOURCE
rm -r CS_build
mkdir CS_build
cd CS_build
../code_saturne-master/configure --prefix=$HOME/CS
make -j
# INSTALL BINARIES
rm -r ../CS/
make install
)

a =-0.8;
Point(1) = {0,0, 0   };
Point(2) = {1,0, 0   };
Point(3) = {2,0, 0   };
Point(4) = {2,0,-0.05};
Point(5) = {1,0,-0.05};
Point(6) = {0,0,-0.05-a*0.05};

Line(1) = {1,2}; Transfinite Line {1} = 51;
Line(2) = {2,5}; Transfinite Line {2} = 6;
Line(3) = {5,6}; Transfinite Line {3} = 51;
Line(4) = {6,1}; Transfinite Line {4} = 6;
Line Loop(1) = {1:4};
Ruled Surface(1) = {1};
Transfinite Surface{1} = {1,2,5,6};
Recombine Surface{1};
Line(5) = {2,3}; Transfinite Line {5} = 51;
Line(6) = {3,4}; Transfinite Line {6} = 6;
Line(7) = {4,5}; Transfinite Line {7} = 51;
Line(8) = {5,2}; Transfinite Line {8} = 6;
Line Loop(2) = {5:8};
Ruled Surface(2) = {2};
Transfinite Surface{2} = {2,3,4,5};
Recombine Surface{2};

out3[] = Extrude{0,0.11,0} {
  Surface{1:2}; Layers{11}; Recombine;
};
Physical Volume("internal") = {1:2};
Physical Surface("Symmetry_planes") = {1,2,30,52};
Physical Surface("Top") = {17,39};
Physical Surface("Bottom") = {25,47};
Physical Surface("Left") = {29};
Physical Surface("Right") = {43};

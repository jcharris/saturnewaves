x1 = 4.5; // about 3 wavelengths for 1s wave ; may need more later
x2 = 4.6125;
x3 = 4.7475;
x4 = 4.8600;
x5 = 9.36;

// dx = 0.01125

z1 =-0.11;
z2 =-0.90;

Point( 1) = { 0,0, 0};
Point( 2) = {x1,0, 0};
Point( 3) = {x2,0, 0};
Point( 4) = {x3,0, 0};
Point( 5) = {x4,0, 0};
Point( 6) = {x5,0, 0};

Point( 7) = { 0,0,z1};
Point( 8) = {x1,0,z1};
Point( 9) = {x2,0,z1};
Point(10) = {x3,0,z1};
Point(11) = {x4,0,z1};
Point(12) = {x5,0,z1};

Point(13) = { 0,0,z2};
Point(14) = {x1,0,z2};
Point(15) = {x2,0,z2};
Point(16) = {x3,0,z2};
Point(17) = {x4,0,z2};
Point(18) = {x5,0,z2};

Line(1) = {1,7}; Transfinite Line{1} = 10;
Line(2) = {7,8}; Transfinite Line{2} = 400;
Line(3) = {8,2}; Transfinite Line{3} = 10;
Line(4) = {2,1}; Transfinite Line{4} = 400;
Line Loop(1) = {1:4};
Surface(1) = {1};
Transfinite Surface{1} = {1,7,8,2};
Recombine Surface{1};
Line(5) = { 3, 9}; Transfinite Line{5} = 10;
Line(6) = { 9,10}; Transfinite Line{6} = 12;
Line(7) = {10, 4}; Transfinite Line{7} = 10;
Line(8) = { 4, 3}; Transfinite Line{8} = 12;
Line Loop(2) = {5:8};
Surface(2) = {2};
Transfinite Surface{2} = {3,9,10,4};
Recombine Surface{2};
Line( 9) = { 5,11}; Transfinite Line{ 9} = 10;
Line(10) = {11,12}; Transfinite Line{10} = 400;
Line(11) = {12, 6}; Transfinite Line{11} = 10;
Line(12) = { 6, 5}; Transfinite Line{12} = 400;
Line Loop(3) = {9:12};
Surface(3) = {3};
Transfinite Surface{3} = {5,11,12,6};
Recombine Surface{3};

Line(13) = { 7,13}; Transfinite Line{13} = 70;
Line(14) = {13,14}; Transfinite Line{14} = 400;
Line(15) = {14, 8}; Transfinite Line{15} = 70;
Line(16) = { 8, 7}; Transfinite Line{16} = 400;
Line Loop(4) = {13:16};
Surface(4) = {4};
Transfinite Surface{4} = {7,13,14,8};
Recombine Surface{4};
Line(17) = { 8,14}; Transfinite Line{17} = 70;
Line(18) = {14,15}; Transfinite Line{18} = 10;
Line(19) = {15, 9}; Transfinite Line{19} = 70;
Line(20) = { 9, 8}; Transfinite Line{20} = 10;
Line Loop(5) = {17:20};
Surface(5) = {5};
Transfinite Surface{5} = {8,14,15,9};
Recombine Surface{5};
Line(21) = { 9,15}; Transfinite Line{21} = 70;
Line(22) = {15,16}; Transfinite Line{22} = 12;
Line(23) = {16,10}; Transfinite Line{23} = 70;
Line(24) = {10, 9}; Transfinite Line{24} = 12;
Line Loop(6) = {21:24};
Surface(6) = {6};
Transfinite Surface{6} = {9,15,16,10};
Recombine Surface{6};
Line(25) = {10,16}; Transfinite Line{25} = 70;
Line(26) = {16,17}; Transfinite Line{26} = 10;
Line(27) = {17,11}; Transfinite Line{27} = 70;
Line(28) = {11,10}; Transfinite Line{28} = 10;
Line Loop(7) = {25:28};
Surface(7) = {7};
Transfinite Surface{7} = {10,16,17,11};
Recombine Surface{7};
Line(29) = {11,17}; Transfinite Line{29} = 70;
Line(30) = {17,18}; Transfinite Line{30} = 400;
Line(31) = {18,12}; Transfinite Line{31} = 70;
Line(32) = {12,11}; Transfinite Line{32} = 400;
Line Loop(8) = {29:32};
Surface(8) = {8};
Transfinite Surface{8} = {11,17,18,12};
Recombine Surface{8};

Extrude{0,0.59,0} {
  Surface{1:8}; Layers{1}; Recombine;
}

Physical Volume("internal") = {1:8};
Physical Surface("Symmetry") = {1,2,3,4,5,6,7,8,54,76,98,120,142,164,186,208};
Physical Surface("Inlet") = {41,107};
Physical Surface("Outlet") = {93,203};
Physical Surface("Bottom") = {111,133,155,177,199};
Physical Surface("Free") = {53,75,97};
Physical Surface("MoonBottom") = {141,185};
Physical Surface("MoonSides") = {49,63,71,85};

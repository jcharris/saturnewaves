x1 = 0.044;
x2 = 0.125;
x3 = 3.0;
z1 =-0.19;
z2 =-0.1943;
z3 =-0.7;

Point( 1) = {x1,0,0}; // free surface
Point( 2) = {x2,0,0};
Point( 3) = {x3,0,0};
Point( 4) = {x1,0,z1}; // above plate
Point( 5) = {x2,0,z1};
Point( 6) = {x3,0,z1};
Point( 7) = { 0,0,z2}; // below plate
Point( 8) = {x2,0,z2};
Point( 9) = {x3,0,z2};
Point(10) = { 0,0,z3}; // seabed
Point(11) = {x2,0,z3};
Point(12) = {x3,0,z3};

Line(1) = {1,4}; Transfinite Line{1} = 44;
Line(2) = {4,5}; Transfinite Line{2} = 19;
Line(3) = {5,2}; Transfinite Line{3} = 44;
Line(4) = {2,1}; Transfinite Line{4} = 19;
Line Loop(1) = {1:4};
Surface(1) = {1};
Transfinite Surface{1} = {1,4,5,2};
Recombine Surface{1};
Line(5) = {2,5}; Transfinite Line{5} = 44;
Line(6) = {5,6}; Transfinite Line{6} = 80 Using Progression 1.05;
Line(7) = {6,3}; Transfinite Line{7} = 44;
Line(8) = {3,2}; Transfinite Line{8} = 80 Using Progression 1/1.05;
Line Loop(2) = {5:8};
Surface(2) = {2};
Transfinite Surface{2} = {2,5,6,3};
Recombine Surface{2};
Line( 9) = {5,8}; Transfinite Line{ 9} = 1;
Line(10) = {8,9}; Transfinite Line{10} = 80 Using Progression 1.05;
Line(11) = {9,6}; Transfinite Line{11} = 1;
Line(12) = {6,5}; Transfinite Line{12} = 80 Using Progression 1/1.05;
Line Loop(3) = {9:12};
Surface(3) = {3};
Transfinite Surface{3} = {5,8,9,6};
Recombine Surface{3};
Line(13) = { 7,10}; Transfinite Line{13} =118;
Line(14) = {10,11}; Transfinite Line{14} = 29;
Line(15) = {11, 8}; Transfinite Line{15} =118;
Line(16) = { 8, 7}; Transfinite Line{16} = 29;
Line Loop(4) = {13:16};
Surface(4) = {4};
Transfinite Surface{4} = {7,10,11,8};
Recombine Surface{4};
Line(17) = { 8,11}; Transfinite Line{17} =118;
Line(18) = {11,12}; Transfinite Line{18} =80 Using Progression 1.05;
Line(19) = {12, 9}; Transfinite Line{19} =118;
Line(20) = { 9, 8}; Transfinite Line{20} =80 Using Progression 1/1.05;
Line Loop(5) = {17:20};
Surface(5) = {5};
Transfinite Surface{5} = {8,11,12,9};
Recombine Surface{5};

Extrude{{0,0,1},{0,0,0},Pi/4/8}{Surface{1:5};Layers{1};Recombine;}

Physical Volume("internal") = {1:5}; // 580

Physical Surface("Cylinder") = {29};
Physical Surface("Plate") = {33,73,102};
Physical Surface("Free") = {41,63};
Physical Surface("Bottom") = {95,116};
Physical Surface("Outside") = {59,120};

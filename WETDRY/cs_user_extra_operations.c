/*============================================================================
 * This function is called at the end of each time step, and has a very
 *  general purpose
 *  (i.e. anything that does not have another dedicated user subroutine)
 *============================================================================*/

/* VERS */

/*
  This file is part of Code_Saturne, a general-purpose CFD tool.

  Copyright (C) 1998-2019 EDF S.A.

  This program is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation; either version 2 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
  Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/*----------------------------------------------------------------------------*/

#include "cs_defs.h"

/*----------------------------------------------------------------------------
 * Standard C library headers
 *----------------------------------------------------------------------------*/

#include <assert.h>
#include <math.h>

#if defined(HAVE_MPI)
#include <mpi.h>
#endif

/*----------------------------------------------------------------------------
 * PLE library headers
 *----------------------------------------------------------------------------*/

#include <ple_coupling.h>

/*----------------------------------------------------------------------------
 *  Local headers
 *----------------------------------------------------------------------------*/

#include "bft_mem.h"
#include "bft_error.h"
#include "bft_printf.h"

#include "fvm_writer.h"

#include "cs_base.h"
#include "cs_field.h"
#include "cs_field_pointer.h"
#include "cs_field_operator.h"
#include "cs_mesh.h"
#include "cs_mesh_quantities.h"
#include "cs_halo.h"
#include "cs_halo_perio.h"
#include "cs_log.h"
#include "cs_notebook.h"
#include "cs_parall.h"
#include "cs_parameters.h"
#include "cs_prototypes.h"
#include "cs_rotation.h"
#include "cs_time_moment.h"
#include "cs_time_step.h"
#include "cs_turbomachinery.h"
#include "cs_selector.h"

#include "cs_post.h"

/*----------------------------------------------------------------------------
 *  Header for the current file
 *----------------------------------------------------------------------------*/

#include "cs_prototypes.h"

#include "solitary.h"

/*----------------------------------------------------------------------------*/

BEGIN_C_DECLS

/*----------------------------------------------------------------------------*/
/*!
 * \file cs_user_extra_operations.c
 *
 * \brief This function is called at the end of each time step, and has a very
 * general purpose (i.e. anything that does not have another dedicated
 * user subroutine)
 *
 */
/*----------------------------------------------------------------------------*/

static FILE *file = NULL;

/*============================================================================
 * User function definitions
 *============================================================================*/

/*----------------------------------------------------------------------------*/
/*!
 * \brief Initialize variables.
 *
 * This function is called at beginning of the computation
 * (restart or not) before the time step loop.
 *
 * This is intended to initialize or modify (when restarted)
 * variable and time step values.

 * \param[in, out]  domain   pointer to a cs_domain_t structure
 */
/*----------------------------------------------------------------------------*/

void
cs_user_extra_operations_initialize(cs_domain_t     *domain)
{
  const cs_mesh_t *m = domain->mesh;
  const cs_mesh_quantities_t *mq = domain->mesh_quantities;
  const int n_cells = m->n_cells;

  if (cs_glob_rank_id <= 0) {
    file = fopen("probes_z.dat","a");
    fprintf(file,"# Time, z_freesurface\n");
  }
  
  cs_real_t height = cs_notebook_parameter_value_by_name("height");
  cs_real_t depth = cs_notebook_parameter_value_by_name("depth");
  cs_real_t H = height/depth;
  solitary_init(H);
}

/*----------------------------------------------------------------------------*/
/*!
 * \brief This function is called at the end of each time step.
 *
 * It has a very general purpose, although it is recommended to handle
 * mainly postprocessing or data-extraction type operations.
 *
 * \param[in, out]  domain   pointer to a cs_domain_t structure
 */
/*----------------------------------------------------------------------------*/

void
cs_user_extra_operations(cs_domain_t     *domain)
{
  const cs_mesh_t *m = domain->mesh;
  const cs_mesh_quantities_t *mq = domain->mesh_quantities;

  const cs_real_3_t *restrict vtx_coord
    = (const cs_real_3_t *restrict)m->vtx_coord;

  const cs_real_3_t *restrict b_face_cog
    = (const cs_real_3_t *restrict)mq->b_face_cog;

  cs_real_3_t *b_face_normal = (cs_real_3_t *)cs_glob_mesh_quantities->b_face_normal;

  const int n_cells = m->n_cells;
  const int n_cells_ext = m->n_cells_with_ghosts;
  const int n_i_faces = m->n_i_faces;
  const int n_b_faces = m->n_b_faces;

  cs_real_t *volume = mq->cell_vol;
  cs_real_3_t *cvar_vel = (const cs_real_3_t *)CS_F_(vel)->val;
  cs_real_t *cpro_rom = (const cs_real_t *)CS_F_(rho)->val;
  for (cs_lnum_t iel = 0; iel < n_cells; iel++) {
    if (volume[iel]<1e-4) {
      cvar_vel[iel][0] = 0.;
      cvar_vel[iel][1] = 0.;
      cvar_vel[iel][2] = 1e-4;
      //      cpro_rom[iel] = 1.0;
    } else {
      //      cpro_rom[iel] = 1.0e3;
    }
  }
  
  const cs_real_t *b_face_surf = (const cs_real_t *)mq->b_face_surf;

  /*===============================================================================
   * 1.  Initialization
   *===============================================================================*/

  cs_lnum_t *face_list;
  BFT_MALLOC(face_list, n_b_faces, cs_lnum_t);

  cs_lnum_t n_faces;
  cs_selector_get_b_face_list("3", &n_faces, face_list);

  cs_real_t x_fs = 20.0;
  cs_real_t dx_fs = 0.1;
  
  cs_real_t z_fs = 0.;
  cs_real_t surf_fs = 0.;
  for (cs_lnum_t i = 0; i < n_faces; i++) {
    cs_lnum_t face_id = face_list[i];

    if (b_face_cog[face_id][0] > x_fs-dx_fs && b_face_cog[face_id][0] < x_fs+dx_fs) {
      z_fs +=
        b_face_cog[face_id][2] // warning here gravity is in z dir
        * b_face_normal[face_id][2];
      surf_fs += b_face_normal[face_id][2];
    }

  }

  BFT_FREE(face_list);

  /* Sum and max of values on all ranks (parallel calculations) */

  cs_parall_sum(1, CS_REAL_TYPE, &z_fs);
  cs_parall_sum(1, CS_REAL_TYPE, &surf_fs);

  if (cs_glob_rank_id <= 0) {
    fprintf(file,"%12f %12f\n",domain->time_step->t_cur, z_fs/surf_fs);
  }

}

/*----------------------------------------------------------------------------*/
/*!
 * \brief This function is called at the end of the calculation.
 *
 * It has a very general purpose, although it is recommended to handle
 * mainly postprocessing or data-extraction type operations.

 * \param[in, out]  domain   pointer to a cs_domain_t structure
 */
/*----------------------------------------------------------------------------*/

void
cs_user_extra_operations_finalize(cs_domain_t     *domain)
{
  if (cs_glob_rank_id <= 0) {
    fclose(file);
  }
  solitary_free();
}

/*----------------------------------------------------------------------------*/

END_C_DECLS

/*============================================================================
 * User definition of boundary conditions.
 *============================================================================*/

/* Code_Saturne version 6.1-alpha */

/*
  This file is part of Code_Saturne, a general-purpose CFD tool.

  Copyright (C) 1998-2019 EDF S.A.

  This program is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation; either version 2 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
  Street, Fifth Floor, Boston, MA 02110-1301, USA.
 */

/*----------------------------------------------------------------------------*/

#include "cs_defs.h"

/*----------------------------------------------------------------------------
 * Standard C library headers
 *----------------------------------------------------------------------------*/

#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>

/*----------------------------------------------------------------------------
 * Local headers
 *----------------------------------------------------------------------------*/

#include "bft_mem.h"
#include "bft_error.h"
#include "bft_printf.h"

#include "cs_base.h"
#include "cs_boundary_zone.h"
#include "cs_field.h"
#include "cs_field_pointer.h"
#include "cs_field_operator.h"
#include "cs_elec_model.h"
#include "cs_log.h"
#include "cs_mesh.h"
#include "cs_mesh_location.h"
#include "cs_mesh_quantities.h"
#include "cs_mesh_quantities.h"
#include "cs_notebook.h"
#include "cs_parameters.h"
#include "cs_time_step.h"
#include "cs_selector.h"

/*----------------------------------------------------------------------------
 * Header for the current file
 *----------------------------------------------------------------------------*/

#include "cs_prototypes.h"

#include "solitary.h"
#include "cs_physical_constants.h"

/*----------------------------------------------------------------------------*/

BEGIN_C_DECLS

/*=============================================================================
 * Public function definitions
 *============================================================================*/

/*----------------------------------------------------------------------------*/
/*!
 * \brief User definition of boundary conditions
 *
 * \param[in]     nvar          total number of variable BC's
 * \param[in]     bc_type       boundary face types
 * \param[in]     icodcl        boundary face code
 *                                - 1  -> Dirichlet
 *                                - 2  -> convective outlet
 *                                - 3  -> flux density
 *                                - 4  -> sliding wall and u.n=0 (velocity)
 *                                - 5  -> friction and u.n=0 (velocity)
 *                                - 6  -> roughness and u.n=0 (velocity)
 *                                - 9  -> free inlet/outlet (velocity)
 *                                inflowing possibly blocked
 * \param[in]     rcodcl        boundary condition values
 *                                rcodcl(3) = flux density value
 *                                (negative for gain) in W/m2
 */
/*----------------------------------------------------------------------------*/

void
cs_user_boundary_conditions(int         nvar,
                            int         bc_type[],
                            int         icodcl[],
                            cs_real_t   rcodcl[])
{
  const cs_lnum_t *b_face_cells = cs_glob_mesh->b_face_cells;
  const cs_lnum_t n_b_faces = cs_glob_mesh->n_b_faces;
  const cs_real_3_t  *restrict b_face_cog
    = (const cs_real_3_t *restrict)cs_glob_mesh_quantities->b_face_cog;
  cs_lnum_t *lstelt = NULL;
  cs_lnum_t *face_list;
  cs_lnum_t  nelts;
  cs_field_t *fu = CS_F_(vel);
  cs_field_t *fp = CS_F_(p);
  const int keyvar = cs_field_key_id("variable_id");
  int ivar_Ux = cs_field_get_key_int(fu, keyvar) - 1 + 0;  //var for Ux
  int ivar_Uy = cs_field_get_key_int(fu, keyvar) - 1 + 1;  //var for Uy
  int ivar_Uz = cs_field_get_key_int(fu, keyvar) - 1 + 2;  //var for Uz
  int ivar_pr = cs_field_get_key_int(fp, keyvar) - 1;  //var for pr
  BFT_MALLOC(lstelt, n_b_faces, cs_lnum_t);
  // Inlet
  cs_real_t xc,yc;

  cs_real_t Tdelay = cs_notebook_parameter_value_by_name("Tdelay");
  cs_real_t depth = cs_notebook_parameter_value_by_name("depth");
  cs_real_t g = fabs(cs_glob_physical_constants->gravity[2]);
  
  cs_selector_get_b_face_list("X0", &nelts, lstelt);
  for (cs_lnum_t ilelt = 0; ilelt < nelts; ilelt++) {
    cs_lnum_t face_id = lstelt[ilelt];
    bc_type[face_id] = CS_INLET;
    xc = b_face_cog[face_id][0]-s.Fr*(cs_glob_time_step->t_cur-Tdelay)*sqrt(g*depth);
    yc = b_face_cog[face_id][2]/depth;
    //define Ux
    icodcl[(ivar_Ux)*n_b_faces+face_id] = 1;
    rcodcl[(ivar_Ux)*n_b_faces+face_id] = solitary_u(xc,yc)*sqrt(g*depth);
    //define Uy
    icodcl[(ivar_Uy)*n_b_faces+face_id] = 1;
    rcodcl[(ivar_Uy)*n_b_faces+face_id] = 0.;
    //define Uz
    icodcl[(ivar_Uz)*n_b_faces+face_id] = 1;
    rcodcl[(ivar_Uz)*n_b_faces+face_id] = solitary_v(xc,yc)*sqrt(g*depth);
    //define pressure
    //icodcl[(ivar_pr)*n_b_faces+face_id] = -1; // the dynamic part P_tilde = P - rho0 g (z-z_0) - p0
    //rcodcl[(ivar_pr)*n_b_faces+face_id] = solitary_p(xc,yc) * g * 1000.;
  }
  BFT_FREE(lstelt);
}

/*----------------------------------------------------------------------------*/

END_C_DECLS

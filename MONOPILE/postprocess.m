% set t_model, f_model from input files
dt = .01;
period = 0.7020;
R = .03;
A = .0245/2;
rho = 1000;
g = 9.81;

[t,fx,fy,fz] = textread('force.txt','%f%f%f%f');

tmax = max(t)-period;
t0_theory = 0:.1*period:tmax;
N_t0 = length(t0_theory);
for i0=1:N_t0
  t0=t0_theory(i0);
  t_interp=t0+(0:1023)/1024*period;
  f_interp = interp1(t,fx,t_interp,"spline");
  F_interp = fft(f_interp);
  F0(i0)=abs(F_interp(1)/1024);
  F1(i0)=abs(F_interp(2)/512);
  F2(i0)=abs(F_interp(3)/512);
  F3(i0)=abs(F_interp(4)/512);
endfor
clf;
hold on;
plot(t0_theory/period,F1/g/rho/R/R/A,'k');
plot(t0_theory/period,F2/g/rho/R/A/A,'b');
plot(t0_theory/period,F3/g/rho/A/A/A,'g');
hold off;

ETA=[t0_theory'/period,F0',F1'/g/rho/R/R/A,F2'/g/rho/R/A/A,F3'/g/rho/A/A/A];
ETA(isnan(ETA))=0;
save('stft.txt','ETA','-ascii');

% set t0
t0 = 7*period;
nt = 4;
t_interp=t0+(0:1023)/1024*period*nt;
f_interp = interp1(t,fx,t_interp,"spline");
F_interp = fft(f_interp); 
printf('For t0/T = %f with model:\n',t0/period);
printf('f1: %f\n',abs(F_interp(1+nt))/512/g/rho/R/R/A);
printf('f2: %f\n',abs(F_interp(1+2*nt))/512/g/rho/R/A/A);
printf('f3: %f\n',abs(F_interp(1+3*nt))/512/g/rho/A/A/A);


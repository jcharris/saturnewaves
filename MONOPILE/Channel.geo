L = 1.5;
r1 = .25;
r2 = .03/Sqrt(2);
n = 30;
n2 = n;
progr = 1.1;

// exterior square
Point(1) = {L+r1+0,0,0};
Point(2) = {L+r1+r1,r1,0};
Point(3) = {L+r1-r1,r1,0};
Point(4) = {L+r1-r1,-r1,0};
Point(5) = {L+r1+r1,-r1,0};
Line(1) = {2,3};
Line(2) = {3,4};
Line(3) = {4,5};
Line(4) = {5,2};

// interior circle
Point(102) = {L+r1+r2,r2,0};
Point(103) = {L+r1-r2,r2,0};
Point(104) = {L+r1-r2,-r2,0};
Point(105) = {L+r1+r2,-r2,0};
Circle(29) = {103,1,102};
Circle(30) = {102,1,105};
Circle(31) = {105,1,104};
Circle(32) = {104,1,103};

// connect sphere and cube
Line(52) = {102,2};
Line(53) = {103,3};
Line(54) = {104,4};
Line(55) = {105,5};

Transfinite Line {52,53,54,55} = n Using Progression progr;
Transfinite Line {1,2,3,4,29,30,31,32} = n2; 

Line Loop(60) = {52,1,-53,29};Ruled Surface(61) = {60};
Line Loop(62) = {53,2,-54,32};Ruled Surface(63) = {62};
Line Loop(64) = {54,3,-55,31};Ruled Surface(65) = {64};
Line Loop(66) = {55,4,-52,30};Ruled Surface(67) = {66};

Point(201) = {0,-r1,0};
Point(202) = {L,-r1,0};
Point(203) = {L,r1,0};
Point(204) = {0,r1,0};
Line(201) = {201,202};
Line(202) = {202,203};
Line(203) = {203,204};
Line(204) = {204,201};
Transfinite Line{201,203} = 50;
Transfinite Line{202,204} = n;
Line Loop(211) = {201,202,203,204};Ruled Surface(211) = {211};

Point(205) = {L+2*r1+0,-r1,0};
Point(206) = {L+2*r1+L,-r1,0};
Point(207) = {L+2*r1+L,r1,0};
Point(208) = {L+2*r1+0,r1,0};
Line(205) = {205,206};
Line(206) = {206,207};
Line(207) = {207,208};
Line(208) = {208,205};
Transfinite Line{205,207} = n;
Transfinite Line{206,208} = n;
Line Loop(212) = {205,206,207,208};Ruled Surface(212) = {212};

n = 11;
r = 1./0.8;
a = (r - 1) / (r^n - 1);
one[0] = 1;
layer[0] = a;
For i In {1:n-1}
  one[i] = 1;
  layer[i] = layer[i-1] + a * r^i;
EndFor
//Extrude {0, 0, 0.6} { Surface{61,63,65,67,211,212}; Layers{16}; Recombine;}
Extrude {0, 0, -0.6} { Surface{61,63,65,67,211,212}; Layers{one[],layer[]}; Recombine;}

Transfinite Surface "*";
Recombine Surface "*";

Physical Volume("internal") = {1:6}; // ext volume
Physical Surface("Symmetry_planes") = {331,225,339,317,309,269};
Physical Surface("Top") = {61,63,65,67,211,212};
Physical Surface("Bottom") = {322,234,256,344,300,278};
Physical Surface("Left") = {321};
Physical Surface("Right") = {335};
Physical Surface("Body") = {233,255,299,277};

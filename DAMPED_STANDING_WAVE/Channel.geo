nx = 128;
ny = 1;
nz = 64;
lx = 2;
ly = 1;
lz =-1;

Point(1) = {0,0,0};
out1[] = Extrude{lx,0,0} {
  Point{1}; Layers{nx};
};
out2[] = Extrude{0,ly,0} {
  Line{1}; Layers{ny}; Recombine;
};
out3[] = Extrude{0,0,lz} {
  Surface{5}; Layers{nz}; Recombine;
};
Physical Volume("internal") = {1};
Physical Surface("Symmetry_planes") = {14,22};
Physical Surface("Top") = {5}; // flipped for negative height
Physical Surface("Bottom") = {27};
Physical Surface("Left") = {26};
Physical Surface("Right") = {18};

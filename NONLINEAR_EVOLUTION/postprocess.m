nx = 801;
lx = 40;
x = linspace(0,lx,nx);
fdir = '../RESU/20230515-1519/postprocessing/';
%dt = 0.0625*4;
dt = 0.0125*4;
period = 2.5;

niter = 50;
iter0 = 1950;
eta = zeros(niter,nx);
for i=1:niter
  iter = iter0+i;
  fname = sprintf('results_boundary.mesh_displacement.%05d',iter);
  fid = fopen(strcat(fdir,fname));
  tmp = fread(fid,244);
  xyz = fread(fid,'single');
  fclose(fid);
  N = length(xyz)/3;
  dz = xyz(2*N+1:end);
  eta(i,:) = dz(end-nx+1:end);
end
Feta = fft(eta,niter,1);


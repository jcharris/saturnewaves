nx = 331;
ny = 301;
nz = 7;
xmin = 128.0;
xmax = 150.0;
ymin =  25.0;
ymax =  45.0;

Point(1) = {xmin,ymin,-1};
out1[] = Extrude{xmax-xmin,0,0} {
  Point{1}; Layers{nx};
};
out2[] = Extrude{0,ymax-ymin,0} {
  Line{1}; Layers{ny}; Recombine;
};
out3[] = Extrude{0,0,1} {
  Surface{5}; Layers{nz}; Recombine;
};
Physical Volume("internal") = {1};
Physical Surface("Symmetry_planes") = {14,22};
Physical Surface("Top") = {5}; // flipped for negative height
Physical Surface("Bottom") = {27};
Physical Surface("Left") = {26};
Physical Surface("Right") = {18};

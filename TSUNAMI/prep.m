fname = 'GMRTv4_1_1_20230520topo.asc';
%fname = 'GMRTv4_1_1_20230519topo.asc';
fid = fopen(fname);
ncols = str2num(fgetl(fid)(7:end));
nrows = str2num(fgetl(fid)(7:end));
xll = str2num(fgetl(fid)(11:end));
yll = str2num(fgetl(fid)(11:end));
dx = str2num(fgetl(fid)(10:end));
fgetl(fid);
dep =-flipud(dlmread(fid,' '));
lon = xll+(1:ncols)*dx; % off by 1?
lat = yll+(1:nrows)*dx;
fclose(fid);

depmin = 10;
Rearth = 6371e3;

latmin = 25;
latmax = 45;
lonmin = 128;
lonmax = 150;
nlat = 301;
nlon = 331;

lon0 = linspace(lonmin,lonmax,nlon);
lat0 = linspace(latmin,latmax,nlat);
[Lon0,Lat0]=meshgrid(lon0,lat0);

Dep0 = interp2(lon,lat,dep,Lon0,Lat0,'nearest');
[Dep0x,Dep0y] = gradient(Dep0,1/60);
Dep0x = Dep0x ./ (Rearth/360*2*pi*sin(Lat0*pi/180));
Dep0y = Dep0y / (Rearth/360*2*pi);

fault_lat0 = [38.862 38.424 37.986 37.547 36.325 35.905 39.398 38.96 38.522 38.084 37.646 39.058 38.62 38.182 37.744 37.331 36.926 36.521 36.101 38.718 38.28];
fault_lon0 = [144.069 143.939 143.81 143.682 142.74 142.504 143.651 143.523 143.397 143.271 143.146 142.977 142.853 142.731 142.609 142.333 142.009 141.684 141.454 142.309 142.19];
fault_depth0 = [0 0 0 0 0 0 12.1 12.1 12.1 12.1 12.1 24.2 24.2 24.2 24.2 24.2 24.2 24.2 24.2 36.3 36.3]*1000;
fault_slip = [5.66 41.15 47.93 8.44 1.89 0.63 0.81 10.67 27.84 33.79 24.11 4.86 19.56 23.38 13.13 11.13 2.23 2.25 0.54 14.64 9.46];

fault_length = [50]*1000*(1+0*fault_lat0);
fault_width = [50]*1000*(1+0*fault_lat0);
fault_strike = [193]*(1+0*fault_lat0);
fault_dip = [14]*(1+0*fault_lat0);
fault_rake = [81]*(1+0*fault_lat0);

uE = 0*Lat0;
uN = 0*Lat0;
uZ = 0*Lat0;
for i=1:length(fault_lat0)
  [dE,dN] = ll2tm(Lat0,Lon0,fault_lat0(i),fault_lon0(i));
  dxi = fault_length(i)/2*sin(pi/180*fault_strike(i)) ...
   +fault_width(i)/2*cos(pi/180*fault_dip(i))*sin(pi/180*(fault_strike(i)+90));
  dyi = fault_length(i)/2*cos(pi/180*fault_strike(i)) ...
   +fault_width(i)/2*cos(pi/180*fault_dip(i))*cos(pi/180*(fault_strike(i)+90));
  [uEi,uNi,uZi] = okada85(dE-dxi,dN-dyi,...
    fault_depth0(i) + fault_width(i)/2*sin(pi/180*fault_dip(i)),...
    fault_strike(i),fault_dip(i),...
    fault_length(i),fault_width(i),fault_rake(i),fault_slip(i),0.0);
  uE = uE + uEi;
  uN = uN + uNi;
  uZ = uZ + uZi;
endfor
Z0 = uZ + uE.*Dep0x + uN.*Dep0y;

midlat = (latmin + latmax) / 2;
midlon = (lonmin + lonmax) / 2;

fid = fopen('Channel.msh');
fid2= fopen('Japan.msh','w');
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);

myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);

myline=fgetl(fid); node_N = str2num(myline); fprintf(fid2,'%s\n',myline);
myi = 0;
ind = zeros(node_N,1);
for i=1:node_N
  x = str2num(fgetl(fid));
  x0 = x(2); y0 = x(3); z0 = x(4);
  mydep = interp2(Lon0,Lat0,Dep0,x0,y0);
  if (mydep>depmin)
    myeta = interp2(Lon0,Lat0,Z0,x0,y0);
    myi = myi + 1;
    ind(i) = myi;
    x(1) = myi;
    [E,N] = ll2tm(y0,x0,midlat,midlon);
    x(2) = E;
    x(3) = N;
    x(4) = mydep*z0+myeta*(1+z0);
    fprintf(fid2,'%s\n',num2str(x));
  end
end

myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
myline=fgetl(fid); elem_N = str2num(myline); fprintf(fid2,'%s\n',myline);

for i=1:elem_N
  sea = 1;
  x = str2num(fgetl(fid));
  nnode = length(x)-5;
  for j=1:nnode
    x(5+j) = ind(x(5+j));
    if (x(5+j)==0)
      sea = 0;
    end
  end
  if (sea==1)
    fprintf(fid2,'%s\n',num2str(x));
  end
end

myline=fgetl(fid); fprintf(fid2,'%s\n',myline);
fclose(fid);
fclose(fid2);

disp(node_N);
disp(elem_N);

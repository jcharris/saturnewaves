nx0 = 256;
nx1 = 8;
nx2 = 128;
ny0 = 10;
ny1 = 22;
ny2 = 10;
nz = 7;

xmin  = -2;   // left BC
xmax  = +2;   // right BC
xbend = 0.34; // where bend starts
W = 0.09;      // width of channel
L = W*3.2;    // ymax
h = 0.05;      // depth

Point(1) = {xmin,0,0};
Extrude{xbend-xmin,0,0} { Point{1}; Layers{nx0};}
Extrude{0,W,0} { Line{1}; Layers{ny0}; Recombine;}
Extrude{0,0,-h} { Surface{5}; Layers{nz}; Recombine;}

Point(7) = {xbend,0,0};
Extrude{W,W, 0} { Point{7}; Layers{nx1};}
Extrude{0,L-W, 0} { Line{28}; Layers{ny1}; Recombine;}
Extrude{0,0,-h} { Surface{32}; Layers{nz}; Recombine;}

Point(7) = {xbend+W,L-W,0};
Extrude{xmax-xbend-W,0,0} { Point{7}; Layers{nx2};}
Extrude{0,W,0} { Line{55}; Layers{ny2}; Recombine;}
Extrude{0,0,-h} { Surface{59}; Layers{nz}; Recombine;}

Physical Volume("internal") = {1:3};
Physical Surface("Top") = {5,32,59};
Physical Surface("Bottom") = {27,54,81};
Physical Surface("Inlet") = {26};

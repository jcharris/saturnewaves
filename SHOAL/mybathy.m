% to test
% ymax = 0.1
% z layers, 0.8 progression

x = linspace(0,30,601);
y = linspace(0,20,201);
[X,Y]=meshgrid(x,y);

XP = (X-10.5)*cos(20*pi/180)-(Y-10)*sin(20*pi/180);
YP = (X-10.5)*sin(20*pi/180)+(Y-10)*cos(20*pi/180);

H = 0*X+0.45;
I = XP>-5.82;
H(I)=0.45-0.02*(5.82+XP(I));
I = (XP/3).^2+(YP/4).^2 < 1;
H(I) = H(I)-0.5*sqrt(1-(XP(I)/3.75).^2-(YP(I)/5.0).^2)+0.3;
I = H<0.07;
H(I) = 0.07; % min depth

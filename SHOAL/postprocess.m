close all;
clear all;

fdir = '../RESU/20230518-1358/postprocessing/';

iter=1;
fname = sprintf('results_boundary.vtx_coord0.%05d',iter);
fid = fopen(strcat(fdir,fname));
tmp = fread(fid,244);
xyz0 = fread(fid,'single');
fclose(fid);
n = length(xyz0)/3;
x0 = xyz0((1:n)+n*0);
y0 = xyz0((1:n)+n*1);
z0 = xyz0((1:n)+n*2);
nx = find(diff(x0)<0)(1);    % 601
ny = find(diff(y0)<0)(1)/nx; % 201
xs = reshape(x0(end-nx*ny+1:end),nx,ny);
ys = reshape(y0(end-nx*ny+1:end),nx,ny);
zmin = 0*xs;
zmax = 0*xs;

for iter=241:270
  fname = sprintf('results_boundary.mesh_displacement.%05d',iter);
  fid = fopen(strcat(fdir,fname));
  tmp = fread(fid,244);
  xyz = fread(fid,'single');
  fclose(fid);
  dx = xyz((1:n)+n*0);
  dy = xyz((1:n)+n*1);
  dz = xyz((1:n)+n*2);
  zs = reshape(dz(end-nx*ny+1:end),nx,ny);
  zmin = min(zmin,zs);
  zmax = max(zmax,zs);
end

% section 1) x:=1m
% section 2) x:=3m
% section 3) x:=5m
% section 4) x:=7m
% section 5) x:=9m
% section 6) y:=-2m 	j=80
% section 7) y:= 0m 	j=100
% section 8) y:=+2m 	j=120

load section1.dat;    %experimental data with amplitude in mm
load section2.dat;
load section3.dat;
load section4.dat;
load section5.dat;
load section678.dat; 

wh = zmax-zmin;
xshift = 4;

figure;
plot(section1(:,1),section1(:,2)*2,'bo','MarkerSize',3.5);
hold;
plot(ys(231,:)-10,wh(231,:)*1000);
axis([-5,5,10,80]);
xlabel('x(m)');
ylabel('H(mm)');

figure;
plot(section2(:,1),section2(:,2)*2,'bo','MarkerSize',3.5);
hold;
plot(ys(271,:)-10,wh(271,:)*1000);
axis([-5,5,10,80]);
xlabel('x(m)');
ylabel('H(mm)');

figure;
plot(section3(:,1),section3(:,2)*2,'bo','MarkerSize',3.5);
hold;
plot(ys(311,:)-10,wh(311,:)*1000);
axis([-5,5,10,120]);
xlabel('x(m)');
ylabel('H(mm)');

figure;
plot(section4(:,1),section4(:,2)*2,'bo','MarkerSize',3.5);
hold;
plot(ys(351,:)-10,wh(351,:)*1000);
axis([-5,5,-5,120]);
xlabel('x(m)');
ylabel('H(mm)');

figure;
plot(section5(:,1),section5(:,2)*2,'bo','MarkerSize',3.5);
hold;
plot(ys(391,:)-10,wh(391,:)*1000);
axis([-5,5,5,120]);
xlabel('x(m)');
ylabel('H(mm)');

figure;
plot(-section678(:,1),section678(:,2)*2,'bo','MarkerSize',3.5);
hold;
plot(xs(:,81)-10.5,wh(:,81)*1000);
axis([0,11,5,120]);
xlabel('x(m)');
ylabel('H(mm)');

figure;
plot(-section678(:,1),section678(:,3)*2,'bo','MarkerSize',3.5);
hold;
plot(xs(:,101)-10.5,wh(:,101)*1000);
axis([0,11,5,120]);
xlabel('x(m)');
ylabel('H(mm)');

figure;
plot(-section678(:,1),section678(:,4)*2,'bo','MarkerSize',3.5);
hold;
plot(xs(:,121)-10.5,wh(:,121)*1000);
axis([0,11,5,120]);
xlabel('x(m)');
ylabel('H(mm)');

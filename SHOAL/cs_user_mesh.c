/*============================================================================
 * Definition of the calculation mesh.
 *
 * Mesh modification function examples.
 *============================================================================*/

/* VERS */

/*
  This file is part of code_saturne, a general-purpose CFD tool.

  Copyright (C) 1998-2022 EDF S.A.

  This program is free software; you can redistribute it and/or modify it under
  the terms of the GNU General Public License as published by the Free Software
  Foundation; either version 2 of the License, or (at your option) any later
  version.

  This program is distributed in the hope that it will be useful, but WITHOUT
  ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
  FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
  details.

  You should have received a copy of the GNU General Public License along with
  this program; if not, write to the Free Software Foundation, Inc., 51 Franklin
  Street, Fifth Floor, Boston, MA 02110-1301, USA.
*/

/*----------------------------------------------------------------------------*/

#include "cs_defs.h"

/*----------------------------------------------------------------------------
 * Standard C library headers
 *----------------------------------------------------------------------------*/

#include <assert.h>
#include <math.h>
#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

/*----------------------------------------------------------------------------
 * Local headers
 *----------------------------------------------------------------------------*/

#include "cs_headers.h"

/*----------------------------------------------------------------------------*/

BEGIN_C_DECLS

/*----------------------------------------------------------------------------*/
/*!
 * \file cs_user_mesh.c
 *
 * \brief Mesh modification example.
 *
 * See \ref cs_user_mesh for examples.
 */
/*----------------------------------------------------------------------------*/

/*============================================================================
 * User function definitions
 *============================================================================*/

/*----------------------------------------------------------------------------*/
/*!
 * \brief Modify geometry and mesh.
 *
 * \param[in,out] mesh  pointer to a cs_mesh_t structure
*/
/*----------------------------------------------------------------------------*/

void
cs_user_mesh_modify(cs_mesh_t  *mesh)
{
  cs_lnum_t  vtx_id;
  for (vtx_id = 0; vtx_id < mesh->n_vertices; vtx_id++) {
    double x = mesh->vtx_coord[vtx_id*3];
    double y = mesh->vtx_coord[vtx_id*3 + 1];
    double pi = 4.0*atan(1.0);
    double xp = (x-10.5)*cos(20.*pi/180.)-(y-10.0)*sin(20.*pi/180.);
    double yp = (x-10.5)*sin(20.*pi/180.)+(y-10.0)*cos(20.*pi/180.);
    double h = 0.45;
    if (xp>-5.82) {
      h = 0.45-0.02*(5.82+xp);
    }
    if (pow(xp/3.0,2)+pow(yp/4.0,2)<1.0) {
      h = h-0.5*sqrt(1.-pow(xp/3.75,2)-pow(yp/5.0,2))+0.3;
    }
    if (h<0.07) {
      h = 0.07;
    }
    mesh->vtx_coord[vtx_id*3 + 2] *= h;
    //mesh->vtx_coord[vtx_id*3 + 2] *= 0.45; // flat to test wavemaker
  }
  /* Set mesh modification flag if it should be saved for future re-use. */
  mesh->modified |= CS_MESH_MODIFIED;
}

/*----------------------------------------------------------------------------*/
/*!
 * \brief Apply partial modifications to the mesh after the preprocessing
 *        and initial postprocessing mesh building stage.
 *
 * \param[in,out] mesh  pointer to a cs_mesh_t structure
 * \param[in,out] mesh_quantities pointer to a cs_mesh_quantities_t structure
*/
/*----------------------------------------------------------------------------*/

void
cs_user_mesh_modify_partial(cs_mesh_t             *mesh,
                            cs_mesh_quantities_t  *mesh_quantities)
{
}

/*----------------------------------------------------------------------------*/

END_C_DECLS
